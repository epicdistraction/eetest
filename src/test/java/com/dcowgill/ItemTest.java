package com.dcowgill;

import junit.framework.TestCase;

/**
 * Unit tests for Item Class
 */
public class ItemTest extends TestCase {

  public void testItemCreation() {
    Item item = new Item(1, "Dove Soap", Money.dollars(39.99));
    assertTrue(item.getValue().equals(Money.dollars(39.99)));
  }
}
