package com.dcowgill;

import java.math.BigDecimal;
import junit.framework.TestCase;

/**
 * Unit tests for Cart Class
 */
public class CartTest extends TestCase {

  public void testAC0() {
    Item doveSoap = new Item(1, "Dove Soap", Money.dollars(39.99));
    Cart cart = new Cart();
    cart.addItem(doveSoap);
    assertTrue(cart.getTotalAmount().equals(Money.dollars(39.99)));
  }

  public void testAC1() {
    Cart cart = new Cart();
    Item doveSoap = new Item(1, "Dove Soap", Money.dollars(39.99));
    cart.addItem(doveSoap, 5);
    cart.addItem(doveSoap, 3);
    assertTrue(cart.getTotalAmount().equals(Money.dollars(319.92)));
  }

  public void testAC2(){
    Cart cart = new Cart();
    cart.setSalesTaxRate(new BigDecimal("0.125"));
    Item doveSoap = new Item(1, "Dove Soap", Money.dollars(39.99));
    Item axeDoes = new Item(2, "Axe Deos", Money.dollars(99.99));
    cart.addItem(doveSoap, 2);
    cart.addItem(axeDoes, 2);

    assertTrue(cart.getTaxedAmount().equals(Money.dollars(35.00)));
    assertTrue(cart.getTotalAmount().equals(Money.dollars(314.96)));
  }
}
