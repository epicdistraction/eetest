package com.dcowgill;

import junit.framework.TestCase;

/**
 * Unit tests for Money Class
 */
public class MoneyTest
  extends TestCase {

  public void testAddition() {
    assertTrue(Money.dollars(1).add(Money.dollars(1)).equals(Money.dollars(2)));
    assertTrue(Money.dollars(1.99).add(Money.dollars(1.99)).equals(Money.dollars(3.98)));
  }

  public void testMutableAddition() {
    assertTrue(Money.dollars(1).mutableAdd(Money.dollars(1)).equals(Money.dollars(2)));
  }

  public void testRounding() {
    assertTrue(Money.dollars(1.995).equals(Money.dollars(2)));
    assertTrue(Money.dollars(1.985).equals(Money.dollars(1.99)));
    assertTrue(Money.dollars(1.984).equals(Money.dollars(1.98)));
  }

  public void testMultiplication() {
    assertTrue(Money.dollars(1).multiply(3).equals(Money.dollars(3)));
    assertTrue(Money.dollars(1.99).multiply(3).equals(Money.dollars(5.97)));
  }
}
