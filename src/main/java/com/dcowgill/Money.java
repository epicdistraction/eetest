package com.dcowgill;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

/**
 * Class to handle Monetary values and Rounding. Inspired from Martin Folwer's book: Analysis
 * Pattern.
 */
public class Money {

  private static final Currency USD = Currency.getInstance("USD");
  private static final RoundingMode DEFAULT_ROUNDING = RoundingMode.HALF_EVEN;

  @Getter
  private final Currency currency;
  @Getter
  private BigDecimal value;
  @Getter
  private final RoundingMode rounding;

  Money(BigDecimal value, Currency currency, RoundingMode rounding) {
    this.currency = currency;
    this.value = value.setScale(currency.getDefaultFractionDigits(), rounding);
    this.rounding = rounding;
  }

  Money(BigDecimal value, Currency currency) {
    this(value, currency, DEFAULT_ROUNDING);
  }

  public static Money dollars(int value) {
    return new Money(new BigDecimal(value), USD);
  }

  public static Money dollars(double value) {
    return new Money(new BigDecimal(value), USD);
  }

  public Money add(Money input) throws RuntimeException {
    compareCurrency(input);
    return new Money(value.add(input.getValue()), currency, rounding);
  }

  public Money mutableAdd(Money input) throws RuntimeException {
    value = value.add(input.getValue());
    return this;
  }

  public void compareCurrency(Money input) {
    if (getCurrency() != input.getCurrency()) {
      throw new RuntimeException("Cannot compare different types of currencies");
    }
  }

  public boolean equals(Money input) {
    compareCurrency(input);
    if (getValue().equals(input.getValue())) {
      return true;
    } else {
      return false;
    }
  }

  public Money multiply(int input) {
    return new Money(getValue().multiply(new BigDecimal(input)), getCurrency());
  }

  public Money multiply(BigDecimal input) {
    return new Money(getValue().multiply(input), getCurrency());
  }
}

