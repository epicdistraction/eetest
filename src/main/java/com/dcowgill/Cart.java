package com.dcowgill;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.HashMap;

public class Cart {
  @Getter
  @Setter
  private BigDecimal salesTaxRate;
  private HashMap<Integer, CartItem> cartItems;

  Cart() {
    cartItems = new HashMap<>();
    salesTaxRate = new BigDecimal(0);
  }

  public void addItem(Item item) {
    this.addItem(item, 1);
  }

  public void addItem(Item item, int quantity) {
    CartItem mappedItem = cartItems.get(item.getId());
    if (mappedItem != null) {
      mappedItem.setQuantity(mappedItem.getQuantity() + quantity);
    } else {
      cartItems.put(item.getId(), new CartItem(item, quantity));
    }
  }

  public Money getSubTotal() {
    Money output = Money.dollars(0);
    cartItems.forEach((id, value) -> {
      output.mutableAdd(value.getItem().getValue().multiply(value.getQuantity()));
    });
    return output;
  }

  public Money getTaxedAmount() {
    Money subTotal = getSubTotal();
    return subTotal.multiply(salesTaxRate);
  }

  public Money getTotalAmount() {
    return getSubTotal().add(getTaxedAmount());
  }
}
